package main

import (
	"fmt"
	"io/ioutil"
	"reflect"
	"testing"
)

func BenchmarkCalculateGammaChanges(b *testing.B) {
	var r, g, be []uint16
	for i := 0; i < b.N; i++ {
		r, g, be = calculateGammaChanges(2569, 1, 0.8, 0.55)
	}
	fmt.Fprintln(ioutil.Discard, len(r), len(g), len(be))
}

func BenchmarkCalculateGammaChangesFast(b *testing.B) {
	var r, g, be []uint16
	for i := 0; i < b.N; i++ {
		r, g, be = calculateGammaChangesFast(2569, 1, 0.8, 0.55)
	}
	fmt.Fprintln(ioutil.Discard, len(r), len(g), len(be))
}

func TestCalculateGammaChanges(t *testing.T) {
	er := []uint16{0, 6553, 13107, 19660, 26214, 32767, 39321, 45874, 52428, 58981}
	eg := []uint16{0, 5242, 10485, 15728, 20971, 26214, 31456, 36699, 41942, 47185}
	ebe := []uint16{0, 3604, 7208, 10813, 14417, 18022, 21626, 25230, 28835, 32439}

	r, g, be := calculateGammaChanges(10, 1, 0.8, 0.55)
	fr, fg, fbe := calculateGammaChangesFast(10, 1, 0.8, 0.55)

	if !reflect.DeepEqual(r, er) {
		t.Errorf("%+v not equal to %+v", r, er)
	}

	if !reflect.DeepEqual(r, fr) {
		t.Errorf("%+v not equal to %+v", r, fr)
	}

	if !reflect.DeepEqual(g, fg) {
		t.Errorf("%+v not equal to %+v", g, fg)
	}

	if !reflect.DeepEqual(g, eg) {
		t.Errorf("%+v not equal to %+v", g, eg)
	}

	if !reflect.DeepEqual(be, fbe) {
		t.Errorf("%+v not equal to %+v", be, fbe)
	}

	if !reflect.DeepEqual(be, ebe) {
		t.Errorf("%+v not equal to %+v", be, ebe)
	}
}
