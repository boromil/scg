package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/randr"
	"github.com/BurntSushi/xgb/xproto"
)

func main() {
	if len(os.Args) < 4 {
		log.Fatalln("gamma values not provided")
	}

	gammaR, gammaG, gammaB, err := gammaFromArgs(os.Args[1], os.Args[2], os.Args[3])
	if err != nil {
		log.Fatalf("failed to parse gamma value: %v", err)
	}

	display, err := xgb.NewConn()
	if err != nil {
		log.Fatalf("failed to get display: %v", err)
	}
	defer display.Close()

	if err := randr.Init(display); err != nil {
		log.Fatalf("failed to init randr: %v", err)
	}

	rootWindow := xproto.Setup(display).DefaultScreen(display).Root
	resources, err := randr.GetScreenResources(display, rootWindow).Reply()
	if err != nil {
		log.Fatalf("failed to get screen resources: %v", err)
	}

	if len(resources.Crtcs) == 0 {
		log.Fatalln("no crtcs found")
	}

	for _, crtcs := range resources.Crtcs {
		gammaSizeReq := randr.GetCrtcGammaSize(display, crtcs)
		repy, err := gammaSizeReq.Reply()
		if err != nil {
			log.Printf("failed to get crtc gamma size: %v, %v", crtcs, err)
			continue
		}

		// red, green, blue := calculateGammaChanges(repy.Size, gammaR, gammaG, gammaB)
		red, green, blue := calculateGammaChangesFast(repy.Size, gammaR, gammaG, gammaB)
		setGammaReq := randr.SetCrtcGammaChecked(display, crtcs, repy.Size, red, green, blue)
		if err := setGammaReq.Check(); err != nil {
			log.Printf("failed to set gamma: %v", err)
		}
	}
}

func gammaFromArgs(r, g, b string) (fr float64, fg float64, fb float64, err error) {
	if fr, err = strconv.ParseFloat(r, 64); err != nil {
		err = fmt.Errorf("r: %w", err)
	}
	if fg, err = strconv.ParseFloat(g, 64); err != nil {
		err = fmt.Errorf("g:  %w", err)
	}
	if fb, err = strconv.ParseFloat(b, 64); err != nil {
		err = fmt.Errorf("b: %w", err)
	}

	return fr, fg, fb, err
}

func calculateGammaChanges(size uint16, gammaR, gammaG, gammaB float64) ([]uint16, []uint16, []uint16) {
	red := make([]uint16, size)
	green := make([]uint16, size)
	blue := make([]uint16, size)

	floatSize := float64(size)
	for i := uint16(0); i < size; i++ {
		g := 65535 * float64(i) / floatSize
		red[i] = uint16(g * gammaR)
		green[i] = uint16(g * gammaG)
		blue[i] = uint16(g * gammaB)
	}

	return red, green, blue
}

func calculateGammaChangesFast(size uint16, gammaR, gammaG, gammaB float64) ([]uint16, []uint16, []uint16) {
	red := make([]uint16, size)
	green := make([]uint16, size)
	blue := make([]uint16, size)

	dg := 65535 / float64(size)
	g := float64(0)
	for i := uint16(0); i < size; i++ {
		red[i] = uint16(g * gammaR)
		green[i] = uint16(g * gammaG)
		blue[i] = uint16(g * gammaB)
		g += dg
	}

	return red, green, blue
}
